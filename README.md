Requirements:
community.postgresql.postgresql_db (https://docs.ansible.com/ansible/latest/collections/community/postgresql/postgresql_db_module.html)
nginx_core (https://galaxy.ansible.com/nginxinc/nginx_core)
1. DONE - Create ansible enviroment - hosts, config
2. DONE - Use vagrant to launch ubuntu 18.04
3. DONE - Create role to deploy postgresql 11
4. DONE - Create role to deploy python-sample-app
5. DONE - Create role to deploy nginx.
100. DONE - create systemd script
110. DONE - use ansible-vault
120. try the same on centos 7
======= log
Launch: ansible-playbook supermain.yaml
1. Created symlinks: /etc/ansible/ansible.cfg to ~/ansible-lesson/ansible/ansible.cfg, /etc/ansible/hosts.yaml to ~/ansible-lesson/ansible/hosts.yaml, /etc/ansible/roles. 

2. Launched 3 VM using Libvirt plugin and Vagrant. Spended a lot of time, but i can't figured out how to deploy new VM with static IP or DNS.
Vagrant deploy my ssh key (generated for testlab.local) but do not creates new user, so in ansible and ssh must use vagrant user.

3. Used ansible-galaxy to init catalog structure in ansible-lesson/ansible/roles/postgresql-server and creating new file - supermain.yaml. In roles/postgresql-server/tasks created playbook install-ubuntu.yaml - which add repo key, add repo file to sources.d folder, and installs postgree. Add variable file and variable playbook.

############################################
literature:
https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html
https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html

